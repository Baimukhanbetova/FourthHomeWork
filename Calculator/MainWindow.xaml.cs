﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string firstText;
        string secondText;
        int firstNumber;
        int secondNumber;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ZeroButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "0";
        }

        private void OneButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "1";
        }

        private void TwoButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "2";
        }

        private void ThreeButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "3";
        }

        private void FourButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "4";
        }

        private void FiveButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "5";
        }

        private void SixButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "6";
        }

        private void sevenButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "7";
        }

        private void EightButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "8";
        }

        private void NineButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "9";
        }

        private void PlusButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "+";
        }

        private void MinusButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "-";
        }

        private void MultiplButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "*";
        }

        private void DivisionButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Text = screenTextBox.Text + "/";
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            screenTextBox.Clear();
        }

        private void EqualButton_Click(object sender, RoutedEventArgs e)
        {
            firstText = screenTextBox.Text.ToString();
            secondText = screenTextBox.Text.ToString();
            char plus = '+';
            char minus = '-';
            char division = '/';
            char multipl = '*';
            int secondPlusIndex = secondText.IndexOf(plus);
            int firstPlusIndex = firstText.IndexOf(plus) + 1;
            int firstMinusIndex = firstText.IndexOf(minus) + 1;
            int secondMinusIndex = firstText.IndexOf(minus);
            int firstDivisionIndex = firstText.IndexOf(division) + 1;
            int secondDivisionIndex = firstText.IndexOf(division);
            int firstMultiplIndex = firstText.IndexOf(multipl) + 1;
            int secondMultiplIndex = firstText.IndexOf(multipl);
            if (!(screenTextBox.Text == ""))
            {
                if (screenTextBox.Text.ToString().Contains(plus))
                {
                    RetrievalOfNumbers(firstPlusIndex, secondPlusIndex);
                    screenTextBox.Text = screenTextBox.Text + "=" + (firstNumber + secondNumber);
                }
                else if (screenTextBox.Text.ToString().Contains(minus))
                {
                    RetrievalOfNumbers(firstMinusIndex, secondMinusIndex );
                    screenTextBox.Text = screenTextBox.Text + "=" + (firstNumber - secondNumber);
                }
                else if (screenTextBox.Text.ToString().Contains(division))
                {
                    RetrievalOfNumbers(firstDivisionIndex, secondDivisionIndex);
                    screenTextBox.Text = screenTextBox.Text + "=" + (firstNumber / secondNumber);
                }
                else if (screenTextBox.Text.ToString().Contains(multipl))
                {
                    RetrievalOfNumbers(firstMultiplIndex, secondMultiplIndex);
                    screenTextBox.Text = screenTextBox.Text + "=" + (firstNumber * secondNumber);
                }
            }
        }
        private void RetrievalOfNumbers(int firstIndex, int secondIndex)
        {
            firstText = firstText.Substring(firstIndex);
            secondText = secondText.Substring(0, secondIndex);
            secondNumber = Convert.ToInt32(firstText.ToString());
            firstNumber = Convert.ToInt32(secondText.ToString());
        }

    }
}

